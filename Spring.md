# **Spring MVC Architecture** #
### What is spring MVC Architecture?
Before we proceed with spring MVC, Let's see what is MVC stands for-
<ul>
<li><strong>M</strong> for Model</li>
<li><strong>V</strong> stands for View</li>
<li><strong>C</strong> stands for Controller</li>
</ul>
 Spring MVC is a part of Spring Framework which is used to develop a web-based and distributed application.


### Spring MVC -
Spring MVC is based on -
1. MVC Design Pattern
2. Front Controller Design pattern

### Component in Spring MVC -
1. Front Controller.
2. Handler Mapper
3. Controller
4. ModelAndView
5. ViewResolver
6. View

![An image](https://www.tutorialspoint.com/spring/images/spring_dispatcherservlet.png)

image credit - [tutorialspoint](https://www.tutorialspoint.com/spring/spring_web_mvc_framework.htm)
### Front Controller :
This component is responsible to perform pre-processing and post-processing of an HTTP request.
In Spring MVC **Dispatcher servlet** is our Front Controller which is a pre-defined servlet provided by the Spring MVC module.


### Handler Mapper :
To process request incoming requests we need the controller to process the incoming request. But in an application
there can be a huge number of the controller also known as request handlers.**Then which one to select ?**  So, Front controller will send a request to
Handler Mapper to identify which controller is responsible for incoming requests by using URL patterns. The handler mapper
will identify the request handler (Controller) and returns handler details to DispatcherServlet.


### Controller :
It will handle the request and process it. it can be user-defined or pre-defined
request. After processing the request controller will return the response as ModelAndView object to DispatcherServlet.

### ModelAndView :
**Model** holds the data that is going to be displayed.
**View** is a logical view file name. i.e. where to show the data.

### ViewResolver :
Once the response is returned to DispatcherServlet as ModelAndView object, based on the view.
provided DispatcherServlet will pass that view to ViewResolver. Then based on the view, ViewResolver
will send the location of the view component and its extension to DispatcherServlet.
Now DispatcherServlet knows Data and filename also.


### View :
Now DispatcherServlet send data & view file to View component it will render the data to provided view file
send back to DispatcherServlet.

Now DispatcherServlet will post-process it and send back HTTPRequest to the client.


### Summary of flow of incoming request :
![An image](https://static.javatpoint.com/sppages/images/flow-of-spring-web-mvc.png)

image credit - [javatpoint](https://www.javatpoint.com/spring-mvc-tutorial)

1. Client sends the request.
2. Front Controller i.e. DispatcherServlet will map the incoming request from Handler Mapper.
3. Based on the mapping of the Handler mapper, DispatcherServlet will route the incoming request to that particular controller.
4. Controller will process the incoming request and will return the ModelAndView object to DispatcherServlet.
5. Now View will be sent to ViewResolver to find out exactly where the request should be displayed.
6. Based on the output of ViewResolver, DispatcherServlet will transfer that to the View component to render it.
7. View will prepare the response which will provide it back to DispatcherServlet.
8. After pre-processing response will be given back to the Client.

### Reference:
1. [tutorialspoint](https://www.tutorialspoint.com/spring/spring_web_mvc_framework.htm)
2. [javatpoint](https://www.javatpoint.com/spring-mvc-tutorial)
3. [spring-framework/docs](https://docs.spring.io/spring-framework/docs/3.2.x/spring-framework-reference/html/mvc.html)
4. [Ashok IT - youtube](https://youtu.be/7dr4fOEgM1c) 
